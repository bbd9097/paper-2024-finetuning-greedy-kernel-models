## FINETUNING GREEDY KERNEL MODELS BY EXCHANGE ALGORITHMS


This repository contains the supplementary material for the preprint
```
FINETUNING GREEDY KERNEL MODELS BY EXCHANGE ALGORITHMS (2024)
T. Wenzel, A. ISKE
```

It was used to carry out the numerical experiments and generate the figures for the publication.
The experiments were performed on Linux systems in 2024 and were tested with a Python version that time (e.g., `python3.10`).


## Installation

After cloning the repository, execute the `setup.sh` file.
It will create a virtual environment and install the required tools.
To activate the virtual environment, use `source venv/bin/activate`



## Rerunning experiments and reproducing the plots

To rerun the numerical experiments of Section 4 and reproduce its plots, execute:

- `main_01_insertion_vs_removal.py`
- `main_02a_func_approx_low_compute.py`
- `main_03a_func_approx_high.py`


