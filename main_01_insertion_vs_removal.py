# Comparison of VKOGA and ERBA on a simple 2D function
# THIS IS THE CODE USED FOR THE FIRST NUMERICAL EXPERIMENTS WITHIN THE KERNEL EXCHANGE PAPER.


import time
import numpy as np
from scipy.stats import qmc
from matplotlib import pyplot as plt

from vkoga_2L.kernels import Matern
from vkoga_2L.vkoga_2L import VKOGA_2L
from utilities.erba import ERBA




nr_setting = 1

## Some settings
if nr_setting == 0:
    dim = 2
    f_func = lambda x: x[:, [0]]**2 + x[:, [1]]**2
    kernel = Matern(k=2, flag_normalize_x=True)
    Npoints = 200
elif nr_setting == 1:
    dim = 3
    f_func = lambda x: np.abs(x[:, [0]]-.5) + np.sin(x[:, [1]] + x[:, [2]])
    kernel = Matern(k=1, flag_normalize_x=True)
    Npoints = 200


greedy_type = 'f_greedy'    

## Some more settings
sampler = qmc.Sobol(d=dim, scramble=False)      # use low discrepancy points for increased stability
sample = 2*sampler.random_base2(m=int(np.ceil(np.log(Npoints) / np.log(2))))-1

n_pts_max = sample.shape[0]
maxIter = n_pts_max


## Create data
X_train, X_test = sample, np.random.rand(n_pts_max, dim)
y_train, y_test = f_func(X_train), f_func(X_test)


## Run VKOGA
t0 = time.time()
model_VKOGA = VKOGA_2L(kernel=kernel, greedy_type=greedy_type, verbose=True)
model_VKOGA.fit(X_train, y_train, X_val=X_test, y_val=y_test, maxIter=maxIter)

# Compute final error and append to list
model_VKOGA.train_hist['f'].append(np.max(np.abs(model_VKOGA.predict(X_train) - y_train)**2))
model_VKOGA.train_hist['f val'].append(np.max(np.abs(model_VKOGA.predict(X_test) - y_test)**2))
t1 = time.time()


## Run ERBA
t2 = time.time()
model_ERBA = ERBA(kernel=kernel, greedy_type=greedy_type, verbose=True)
model_ERBA.fit(X_train, y_train, X_val=X_test, y_val=y_test, maxIter=maxIter)
t3 = time.time()


## Visualize some results
list_idx_vkoga = list(np.arange(len(model_VKOGA.train_hist['f'])))
list_idx_erba = list(np.arange(1+len(model_ERBA.train_hist['f'])))

plt.figure(10 + nr_setting)
plt.clf()
plt.plot(list_idx_vkoga[1:], np.sqrt(model_VKOGA.train_hist['f'][1:]), label='VKOGA')
plt.plot(list_idx_erba[1:], np.sqrt(model_ERBA.train_hist['f'][::-1]), label='ERBA')
plt.legend()
plt.xscale('log')
plt.yscale('log')
plt.show(block=False)











