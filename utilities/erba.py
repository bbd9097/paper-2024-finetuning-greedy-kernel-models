# Low effort implementation of ERBA. Mind that this is a very basic implementation suited to the needs of this experiment,
# and not optimized for speed.


from vkoga_2L.kernels import Gaussian
import numpy as np
from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from scipy.spatial import distance_matrix
from datetime import datetime
    
# VKOGA implementation
class ERBA(BaseEstimator):
                                          
    def __init__(self, kernel=Gaussian(), kernel_par=1,
                 verbose=True, n_report=10,
                 greedy_type='p_greedy', 
                 tol_f=1e-10, tol_p=1e-10):
        
        # Set the verbosity on/off
        self.verbose = verbose
        
        # Set the frequency of report
        self.n_report = n_report
        
        # Set the params defining the method 
        self.kernel = kernel
        self.kernel_par = kernel_par
        self.greedy_type = greedy_type

        # Define the greedy method properly
        self.greedy_type = greedy_type

        assert self.greedy_type == 'f_greedy', 'Only f_greedy is implemented so far!'

        # Set the stopping values
        self.tol_f = tol_f
        self.tol_p = tol_p

        # Initialize the convergence history
        self.train_hist = {}
        self.train_hist['n'] = []
        self.train_hist['f'] = []
        self.train_hist['f val'] = []
        
    def fit(self, X, y, X_val=None, y_val=None, maxIter=None):

        # Check the datasets
        X, y = check_X_y(X, y, multi_output=True)
        if len(y.shape) == 1:
            y = y[:, None]

        # Get the data dimension
        N, q = y.shape
        
        # Set maxIter_continue
        if maxIter is None or maxIter > N:
            self.maxIter = 100
        else:
            self.maxIter = maxIter

        # Initialize list for the chosen and non-chosen indices
        indI = list(range(N))
        notIndI = []
        
        # Iterative selection of new points
        for idx_iter in range(self.maxIter):
            
            # Compute kernel model
            A = self.kernel.eval(X[indI, :], X[indI, :])
            invA = np.linalg.inv(A)

            coeff = np.linalg.solve(A, y[indI, :])

            # Compute current training and test error
            y_train_pred = self.kernel.eval(X, X[indI, :]) @ coeff
            error_train = np.max(np.sum((y - y_train_pred) ** 2, axis=1))
            self.train_hist['f'].append(error_train)

            if X_val is not None:
                y_val_pred = self.kernel.eval(X_val, X[indI, :]) @ coeff
                error_val = np.max(np.sum((y_val - y_val_pred) ** 2, axis=1))
                self.train_hist['f val'].append(error_val)

            # Compute error criterion for f-greedy
            array_error = np.abs(coeff / np.diag(invA).reshape(-1,1))           # absolute value is important here!!

            # choose argmin of array_error
            idx = np.argmin(array_error)
            notIndI.append(indI[idx])
            indI.pop(idx)

            if idx_iter % 10 == 0 and self.verbose:
                print(datetime.now().strftime("%H:%M:%S"), 'Calculation for idx_iter={} finished.'.format(idx_iter))

        # Save the final indices
        self.indI = indI
        
        return self

