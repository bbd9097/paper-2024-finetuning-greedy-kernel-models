import numpy as np

class Dataset():
    """
    Implementation of the test functions which were used in the experiments
    """

    def __init__(self, N_points=10000):

        self.N_points = N_points
        self.X = None
        self.y = None

        self.dic_dataset = {
            'example_2d_franke': (lambda x: 0.75 * np.exp(-(9 * x[:, [0]] - 2) ** 2 / 4 - (9 * x[:, [1]]- 2) ** 2 / 4)
                                            + 0.75 * np.exp(-(9 * x[:, [0]] + 1) ** 2 / 49 - (9 * x[:, [1]] + 1) / 10)
                                            + 0.5 * np.exp(-(9 * x[:, [0]] - 7) ** 2 / 4 - (9 * x[:, [1]] - 3) ** 2 / 4)
                                            - 0.2 * np.exp(-(9 * x[:, [0]] - 4) ** 2 - (9 * x[:, [1]] - 7) ** 2), 2),
            'example_F2': (lambda x: 1/9 * (np.tanh(9*x[:, [1]] - 9 * x[:, [0]]) + 1), 2),
            'example_F3': (lambda x: ( 125 / 100 + np.cos(5.4 * x[:, [1]])) / (6 * (1 + (3 * x[:, [0]] - 1)**2)), 2),
            'example_F4': (lambda x: 1/3 * np.exp(- 81/16 * ((x[:, [0]] - 1/2)**2 + (x[:, [1]] - 1/2)**2)), 2),
            'example_5d_faster_conv': (lambda x: np.exp(-4 * np.sum(x[:, :] - .5 * np.ones_like(x[:, :]), axis=1, keepdims=True) ** 2), 5),
            'example_6d_kink': (lambda x: (np.exp(-4 * np.sum((x[:, :] - .5 * np.ones_like(x[:, :])) ** 2, axis=1, keepdims=True)) + 2 * np.abs(x[:, [0]] - .5)), 6),
        }


    def get_data(self, name_dataset):

        X, y = None, None

        if name_dataset in self.dic_dataset.keys():
            fcn, dim = self.dic_dataset[name_dataset]

            X = np.random.rand(self.N_points, dim)
            y = fcn(X)

        return X, y
