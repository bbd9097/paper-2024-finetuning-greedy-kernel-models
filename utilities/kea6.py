# Implementation of kea. Implementation is similar to VKOGA.

import time
import numpy as np

from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted

from vkoga_2L.vkoga_2L import VKOGA_2L
from vkoga_2L.kernels import Gaussian



# KEA implementation
class KEA(BaseEstimator):
    '''
    Implementation of KEA: Kernel Exchange Algorithm.
    '''
                                          
    def __init__(self, X_ctrs, y_ctrs, kernel=Gaussian(), kernel_par=1,
                 verbose=True, n_report=10,
                 exchange_type='p_exchange', reg_par=0,
                 tol_f=1e-10, tol_p=1e-10, beta=None):

        # Initial set of centers and corresponding values
        X_ctrs, y_ctrs = check_X_y(X_ctrs, y_ctrs, multi_output=True)   # ToDo: Think about multi output
        self.X_ctrs, indices = np.unique(X_ctrs, axis=0, return_index=True)
        self.y_ctrs = y_ctrs[indices, :]
        
        # Set the verbosity on/off
        self.verbose = verbose
        
        # Set the frequency of report
        self.n_report = n_report
        
        # Set the params defining the method 
        self.kernel = kernel
        self.kernel_par = kernel_par
        self.exchange_type = exchange_type
        self.reg_par = reg_par

        # Define the greedy method properly
        if beta is None:
            self.exchange_type = exchange_type
            if self.exchange_type == 'f_exchange':
                self.beta = 1.0
            elif self.exchange_type == 'p_exchange':
                self.beta = 0.0
            elif self.exchange_type == 'random':
                self.beta = 1.0  # use any value, should not matter!
        else:
            self.beta = float(beta)
            if self.beta == 0.0:
                self.exchange_type = 'p_exchange'
            elif self.beta == 1.0:
                self.exchange_type = 'f_exchange'
            elif self.beta == np.inf:
                self.exchange_type = 'fp_exchange'
            else:
                self.exchange_type = '{}'.format(beta)

        # ToDo: Did never think about the regularization parameter ..
        assert reg_par == 0, 'reg_par > 0 might not be implemented correctly!'

        self.flag_val = None
        
        # Set the stopping values
        self.tol_f = tol_f
        self.tol_p = tol_p

        # Some further settings
        self.ctrs_ = None
        self.Cut_ = None
        self.c = None


        # Initialize the convergence history
        self.train_hist_add = {}
        self.train_hist_add['f'] = []
        self.train_hist_add['p'] = []
        self.train_hist_add['n'] = []

        self.train_hist_remove = {}
        self.train_hist_remove['f'] = []
        self.train_hist_remove['p'] = []

        
    def selection_rule_add(self, f, p):
        # Selection rule for adding centers

        f = np.sum(f ** 2, axis=1)
        f_max = np.max(f)
        p_max = np.max(p)

        if self.exchange_type == 'f_exchange':
            idx = np.argmax(f)
        elif self.exchange_type == 'fp_exchange':
            idx = np.argmax(f / p)
        elif self.exchange_type == 'p_exchange':
            idx = np.argmax(p)
        elif self.exchange_type == 'rand':    # pick some random point - David Holzmüller asked me about its performance
            idx = np.random.randint(len(p))
        else:
            idx = np.argmax(f ** self.beta * p ** (1-self.beta))

        return idx, f_max, p_max

    def selection_rule_remove(self, f, p):
        # Selection rule for removing centers

        # Compute leave one out cross validation errors
        loocv_res1 = np.abs(self.coef_ / np.diag(self.invA)[:, None])  # computation of loocv
        loocv_pow1 = 1 / np.sqrt(np.diag(self.invA)).reshape(-1, 1)

        f = np.sum(f ** 2, axis=1)
        f_max = np.max(f)
        p_max = np.max(p)

        if self.exchange_type == 'f_exchange':      # use minimal loocv error to select point for exchange!
            idx = np.argmin(loocv_res1)
        elif self.exchange_type == 'fp_exchange':
            idx = np.argmin(loocv_res1 / loocv_pow1)
        elif self.exchange_type == 'p_exchange':
            idx = np.argmin(loocv_pow1)
        elif self.exchange_type == 'rand':    # pick some random point - David Holzmüller asked me about its performance
            idx = np.random.randint(len(p))
        else:
            idx = np.argmin(loocv_res1 ** self.beta * loocv_pow1 ** (1-self.beta))

        return idx, f_max, p_max

    def fit(self, X, y, maxExch=100, flag_debug=False, N_flexibility=0, flag_best_model=False):
        self.t0 = time.time()

        # We assume that we start with an initial set of centers and corresponding values, i.e. X_ctrs, y_ctrs
        X_ctrs = self.X_ctrs
        y_ctrs = self.y_ctrs

        # Make sure that self.X_ctrs is included in X - otherwise add it
        X, indices = np.unique(X, axis=0, return_index=True)            # remove duplicates
        y = y[indices, :]

        X = np.concatenate((X_ctrs, X), axis=0)
        y = np.concatenate((y_ctrs, y), axis=0)

        X, indices_index, indices_inverse = \
            np.unique(X, axis=0, return_index=True, return_inverse=True)

        y = y[indices_index, :]


        indI = list(indices_inverse[:X_ctrs.shape[0]-N_flexibility])        # remove N_flexibility additional centers

        # Introduce a suitable ordering into indI to save computational time later on
        model_greedy = VKOGA_2L(kernel=self.kernel,  greedy_type='f_greedy', verbose=False,
                             tol_p=1e-12, tol_f=1e-12)
        _ = model_greedy.fit(X[indI, :], y[indI, :], maxIter=len(indI))

        # assert model_greedy.ctrs_.shape[0] == len(indI), 'model_greedy did not pick all centers!'
        if model_greedy.ctrs_.shape[0] != len(indI):
            print('Watch out, model_greedy did not pick all centers due to ... (probably points to close to each other)!')

        indI = [indI[idx] for idx in model_greedy.indI_]    # ToDo: Check this ordering whether everything worked out

        # now we can forget about ..., simply use X, y and track indI and notIndI
        del self.X_ctrs, X_ctrs
        del self.y_ctrs, y_ctrs

        # Set up stuff & get predictions to directly jump into loop
        self.ctrs_ = X[indI, :]
        self.N = self.ctrs_.shape[0]
        self.c = model_greedy.c
        self.Cut_ = model_greedy.Cut_
        self.invA = self.Cut_.T @ self.Cut_
        self.coef_ = self.invA @ y[indI, :]

        # Get predictions - here we really need to call .predict which is quite expensive
        p_pred = self.predict_P(X)
        y_pred = self.predict(X)

        list_idx_add = []
        flag_break = False

        list_indI = [indI.copy()]

        for idx_iter in range(maxExch + N_flexibility):
            if flag_break:
                break

            ## Select centers to be added and to be removed
            self.train_hist_add['f'].append([])
            self.train_hist_add['p'].append([])

            self.train_hist_remove['f'].append([])
            self.train_hist_remove['p'].append([])

            # Selection criterion for adding
            idx_add, self.train_hist_add['f'][-1], self.train_hist_add['p'][-1] = \
                self.selection_rule_add(y - y_pred, p_pred)     # use p_pred[notIndI] instead of p_pred?
            indI.append(idx_add)
            list_idx_add.append(idx_add)
            # print(idx_add)

            # Selection criterion for removal   -  only remove in the first maxExch iterations
            idx_remove, self.train_hist_remove['f'][-1], self.train_hist_remove['p'][-1] = \
                self.selection_rule_remove(y - y_pred, p_pred)  # use p_pred[notIndI] instead of p_pred?
            indI.pop(idx_remove)        # remove the idx_remove-th index

            # Implement something
            self.ctrs_ = X[indI, :]
            self.A = self.kernel.eval(self.ctrs_, self.ctrs_)
            self.invA = np.linalg.inv(self.A)
            self.coef_ = self.invA @ y[indI, :]
            self.Cut_ = np.linalg.inv(np.linalg.cholesky(self.A))

            p_pred = self.predict_P(X)
            y_pred = self.predict(X)

            list_indI.append(indI.copy())

            # Stop if there is a loop
            for len_loop in [1, 2, 3, 4, 5]:
                if idx_iter > len_loop:
                    if list_idx_add[-len_loop:] == list_idx_add[-2*len_loop:-len_loop]:
                        for _ in range(20):
                            #   print('loop of length {} detected!'.format(len_loop))
                            continue
                        flag_break = True
                        break


        # If desired, pick the best model
        if flag_best_model:
            idx_best_kea = np.argmin(self.train_hist_add['f'])
            indI = list_indI[idx_best_kea]


        # Add the N_flexibility many centers again
        model_final_add = VKOGA_2L(kernel=self.kernel, greedy_type='f_greedy', verbose=False, tol_f=1e-14, tol_p=1e-14)
        model_final_add.fit(X[indI, :], y[indI, :], maxIter=len(indI))      # use the existing centers
        indI = [indI[idx] for idx in model_final_add.indI_]
        notIndI = [idx for idx in range(X.shape[0]) if idx not in indI]  # required because vkoga removes previously chosen points

        if N_flexibility > 0:   # add N_flexibility many centers (if N_flexibility > 0)
            model_final_add.fit(X[notIndI, :], y[notIndI], maxIter=N_flexibility)
            indI = indI + [notIndI[idx] for idx in model_final_add.indI_]

        # Store these results
        self.ctrs_ = model_final_add.ctrs_
        self.c = model_final_add.c # np.concatenate((model_downdate.c, np.array([[0]])), axis=0)
        self.Cut_ = model_final_add.Cut_
        self.invA = self.Cut_.T @ self.Cut_   # not required
        self.coef_ = self.invA @ y[indI, :]

        # Store some quantities to access intermediate centers
        self.list_indI = list_indI
        self._X = X

        # Set some final quantities
        self.indI = indI
        self.notIndI = notIndI

        self.X_ctrs = X[indI, :]
        self.y_ctrs = y[indI, :]

        return self

    def predict(self, X):
        # Check if fit has been called

        # Validate the input
        X = check_array(X)

        # Compute prediction
        if self.ctrs_.shape[0] > 0:
            prediction = self.kernel.eval(X, self.ctrs_) @ self.coef_
        else:
            prediction = np.zeros((X.shape[0], 1))

        return prediction

    def predict_P(self, X, n=None):
        # Prediction of the power function, copied from pgreedy.py

        # Try to do nothing
        if self.ctrs_ is None or n == 0:
            return self.kernel.diagonal(X)

        # Otherwise check if everything is ok
        # Check is fit has been called
        check_is_fitted(self, 'ctrs_')
        # Validate the input
        X = check_array(X)

        # Decide how many centers to use
        if n is None:
            n = np.atleast_2d(self.ctrs_).shape[0]

        # Evaluate the power function on the input
        p = self.kernel.diagonal(X) - np.sum(
            (self.kernel.eval(X, np.atleast_2d(self.ctrs_)[:n]) @ self.Cut_[:n, :n].transpose()) ** 2, axis=1)

        return p

    def print_message(self, when):
        if self.verbose and when == 'begin':
            print('')
            print('*' * 30 + ' [KEA] ' + '*' * 30)
            print('Training model with')
            print('       |_ kernel              : %s' % self.kernel)
            print('       |_ regularization par. : %2.2e' % self.reg_par)
            print('       |_ restriction par.    : %2.2e' % self.restr_par)
            print('')
            
        if self.verbose and when == 'end':
            print('Training completed with')
            print('       |_ selected points     : %8d / %8d' % (self.train_hist_add['n'][-1], self.ctrs_.shape[0] + self.maxIter))

            if self.flag_val:
                print('       |_ train, val residual : %2.2e / %2.2e,    %2.2e' %
                      (self.train_hist['f'][-1], self.tol_f, self.train_hist['f val'][-1]))
                print('       |_ train, val power fun: %2.2e / %2.2e,    %2.2e' %
                      (self.train_hist['p'][-1], self.tol_p, self.train_hist['p val'][-1]))
            else:
                print('       |_ train residual      : %2.2e / %2.2e' % (self.train_hist['f'][-1], self.tol_f))
                print('       |_ train power fun     : %2.2e / %2.2e' % (self.train_hist['p'][-1], self.tol_p))
                        
        if self.verbose and when == 'track':
            print('Training ongoing with')
            print('       |_ selected points     : %8d / %8d' % (self.train_hist_add['n'][-1], self.ctrs_.shape[0] + self.maxIter))
            if self.flag_val:
                print('       |_ train, val residual : %2.2e / %2.2e,    %2.2e' %
                      (self.train_hist['f'][-1], self.tol_f, self.train_hist['f val'][-1]))
                print('       |_ train, val power fun: %2.2e / %2.2e,    %2.2e' %
                      (self.train_hist['p'][-1], self.tol_p, self.train_hist['p val'][-1]))
            else:
                print('       |_ train residual      : %2.2e / %2.2e' % (self.train_hist['f'][-1], self.tol_f))
                print('       |_ train power fun     : %2.2e / %2.2e' % (self.train_hist['p'][-1], self.tol_p))




