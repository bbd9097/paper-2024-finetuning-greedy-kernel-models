


import os
import numpy as np

import matplotlib.pyplot as plt


path_to_files = 'results/'
list_files = os.listdir(path_to_files)





R = np.linspace(0, 1, int(1.5 * 5))
array_color = plt.cm.hsv(R)


for idx_file, file in enumerate(list_files):


    dic_results = np.load(path_to_files + file, allow_pickle=True).item()
    nr_dataset = int(file.split('_')[-1].split('.')[0])

    if nr_dataset not in [0, 1, 2, 3, 4, 5]:
        continue

    assert nr_dataset == list(dic_results.keys())[0], 'The dataset number should be the same as the key in the dictionary.'
    assert len(dic_results.keys()) == 1, 'There should be only one dataset in the file.'


    list_kernels = list(dic_results[nr_dataset].keys())
    list_nCtrs = list(dic_results[nr_dataset][list_kernels[0]].keys())


    str_error = 'Linfty_test'

    # Visualization of convergence
    plt.figure(100 + idx_file + 1)
    plt.clf()
    for idx_kernel, str_kernel in enumerate(list_kernels):
        if int(str_kernel[-1]) % 2 == 1:
            continue
        plt.plot(list_nCtrs, [dic_results[nr_dataset][str_kernel][nCtrs]['VKOGA'][str_error] for nCtrs in list_nCtrs],
                'x-', color=array_color[idx_kernel, :])
    for idx_kernel, str_kernel in enumerate(list_kernels):
        if int(str_kernel[-1]) % 2 == 1:
            continue
        plt.plot(list_nCtrs, [dic_results[nr_dataset][str_kernel][nCtrs]['KEAi'][str_error] for nCtrs in list_nCtrs],
                'o--', color=array_color[idx_kernel, :])
    plt.xscale('log')
    plt.yscale('log')
    plt.legend([str_kernel for str_kernel in list_kernels if int(str_kernel[-1]) % 2 == 0])
    plt.title('Dataset ' + str(nr_dataset))
    plt.show(block=False)

